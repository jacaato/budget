from PyQt5.QtCore import QPoint, Qt
from PyQt5.QtGui import QPixmap, QPainter, QBrush, QPalette
from PyQt5.QtWidgets import QFrame


class ChartFrame(QFrame):
    def __init__(self, parent=None):
        super(ChartFrame, self).__init__(parent)
        self.image_name = None

    def set_image_name(self, image_name):
        self.image_name = image_name

    def paintEvent(self, QPaintEvent):
        if self.image_name is not None:
            painter = QPainter(self)

            brush = QBrush()
            brush.setStyle(Qt.SolidPattern)
            brush.setColor(self.palette().color(QPalette.Background))
            painter.fillRect(0, 0, self.width(), self.height(), brush)

            pixmap = QPixmap(self.image_name)
            size = min(self.width(), self.height())
            pixmap = pixmap.scaled(size, size, Qt.KeepAspectRatio, Qt.SmoothTransformation)
            painter.drawPixmap(QPoint(self.width() / 2 - pixmap.width() / 2, self.height() / 2 - pixmap.height() / 2),
                               pixmap)
