# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_category.ui'
#
# Created by: PyQt5 UI code generator 5.4.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DialogCategory(object):
    def setupUi(self, DialogCategory):
        DialogCategory.setObjectName("DialogCategory")
        DialogCategory.resize(256, 121)
        self.verticalLayout = QtWidgets.QVBoxLayout(DialogCategory)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(DialogCategory)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.edit_name = QtWidgets.QLineEdit(DialogCategory)
        self.edit_name.setObjectName("edit_name")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.edit_name)
        self.label_2 = QtWidgets.QLabel(DialogCategory)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.combo_parent = QtWidgets.QComboBox(DialogCategory)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.combo_parent.sizePolicy().hasHeightForWidth())
        self.combo_parent.setSizePolicy(sizePolicy)
        self.combo_parent.setObjectName("combo_parent")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.combo_parent)
        self.verticalLayout.addLayout(self.formLayout)
        self.widget = QtWidgets.QDialogButtonBox(DialogCategory)
        self.widget.setOrientation(QtCore.Qt.Horizontal)
        self.widget.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.widget.setObjectName("widget")
        self.verticalLayout.addWidget(self.widget)
        self.label.setBuddy(self.edit_name)
        self.label_2.setBuddy(self.combo_parent)

        self.retranslateUi(DialogCategory)
        self.widget.accepted.connect(DialogCategory.accept)
        self.widget.rejected.connect(DialogCategory.reject)
        QtCore.QMetaObject.connectSlotsByName(DialogCategory)

    def retranslateUi(self, DialogCategory):
        _translate = QtCore.QCoreApplication.translate
        DialogCategory.setWindowTitle(_translate("DialogCategory", "Category"))
        self.label.setText(_translate("DialogCategory", "Na&me"))
        self.label_2.setText(_translate("DialogCategory", "Pa&rent Category"))

