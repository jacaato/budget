# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog_unit.ui'
#
# Created by: PyQt5 UI code generator 5.4.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DialogUnit(object):
    def setupUi(self, DialogUnit):
        DialogUnit.setObjectName("DialogUnit")
        DialogUnit.resize(256, 84)
        self.verticalLayout = QtWidgets.QVBoxLayout(DialogUnit)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(DialogUnit)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.edit_name = QtWidgets.QLineEdit(DialogUnit)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.edit_name.sizePolicy().hasHeightForWidth())
        self.edit_name.setSizePolicy(sizePolicy)
        self.edit_name.setObjectName("edit_name")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.edit_name)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(DialogUnit)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.label.setBuddy(self.edit_name)

        self.retranslateUi(DialogUnit)
        self.buttonBox.accepted.connect(DialogUnit.accept)
        self.buttonBox.rejected.connect(DialogUnit.reject)
        QtCore.QMetaObject.connectSlotsByName(DialogUnit)

    def retranslateUi(self, DialogUnit):
        _translate = QtCore.QCoreApplication.translate
        DialogUnit.setWindowTitle(_translate("DialogUnit", "Unit"))
        self.label.setText(_translate("DialogUnit", "Na&me"))

