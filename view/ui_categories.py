# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'categories.ui'
#
# Created by: PyQt5 UI code generator 5.4.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DockWidgetCategoried(object):
    def setupUi(self, DockWidgetCategoried):
        DockWidgetCategoried.setObjectName("DockWidgetCategoried")
        DockWidgetCategoried.resize(357, 222)
        self.dockWidgetContents = QtWidgets.QWidget()
        self.dockWidgetContents.setObjectName("dockWidgetContents")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.dockWidgetContents)
        self.verticalLayout.setObjectName("verticalLayout")
        self.tableWidget_categories = QtWidgets.QTableWidget(self.dockWidgetContents)
        self.tableWidget_categories.setObjectName("tableWidget_categories")
        self.tableWidget_categories.setColumnCount(2)
        self.tableWidget_categories.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_categories.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_categories.setHorizontalHeaderItem(1, item)
        self.tableWidget_categories.horizontalHeader().setCascadingSectionResizes(True)
        self.tableWidget_categories.horizontalHeader().setSortIndicatorShown(True)
        self.tableWidget_categories.horizontalHeader().setStretchLastSection(True)
        self.tableWidget_categories.verticalHeader().setSortIndicatorShown(False)
        self.tableWidget_categories.verticalHeader().setStretchLastSection(False)
        self.verticalLayout.addWidget(self.tableWidget_categories)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.button_new = QtWidgets.QPushButton(self.dockWidgetContents)
        self.button_new.setObjectName("button_new")
        self.horizontalLayout.addWidget(self.button_new)
        self.button_edit = QtWidgets.QPushButton(self.dockWidgetContents)
        self.button_edit.setObjectName("button_edit")
        self.horizontalLayout.addWidget(self.button_edit)
        self.button_delete = QtWidgets.QPushButton(self.dockWidgetContents)
        self.button_delete.setObjectName("button_delete")
        self.horizontalLayout.addWidget(self.button_delete)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        DockWidgetCategoried.setWidget(self.dockWidgetContents)

        self.retranslateUi(DockWidgetCategoried)
        QtCore.QMetaObject.connectSlotsByName(DockWidgetCategoried)

    def retranslateUi(self, DockWidgetCategoried):
        _translate = QtCore.QCoreApplication.translate
        DockWidgetCategoried.setWindowTitle(_translate("DockWidgetCategoried", "&Categories"))
        item = self.tableWidget_categories.horizontalHeaderItem(0)
        item.setText(_translate("DockWidgetCategoried", "Name"))
        item = self.tableWidget_categories.horizontalHeaderItem(1)
        item.setText(_translate("DockWidgetCategoried", "Parent Category"))
        self.button_new.setText(_translate("DockWidgetCategoried", "New"))
        self.button_edit.setText(_translate("DockWidgetCategoried", "Edit"))
        self.button_delete.setText(_translate("DockWidgetCategoried", "Delete"))

