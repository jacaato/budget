# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'transactions.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DockWidgetTransfers(object):
    def setupUi(self, DockWidgetTransfers):
        DockWidgetTransfers.setObjectName("DockWidgetTransfers")
        DockWidgetTransfers.resize(400, 300)
        self.dockWidgetContents = QtWidgets.QWidget()
        self.dockWidgetContents.setObjectName("dockWidgetContents")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.dockWidgetContents)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(self.dockWidgetContents)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.comboBox_column_filter = QtWidgets.QComboBox(self.dockWidgetContents)
        self.comboBox_column_filter.setObjectName("comboBox_column_filter")
        self.horizontalLayout_2.addWidget(self.comboBox_column_filter)
        self.lineEdit_filter_by = QtWidgets.QLineEdit(self.dockWidgetContents)
        self.lineEdit_filter_by.setObjectName("lineEdit_filter_by")
        self.horizontalLayout_2.addWidget(self.lineEdit_filter_by)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.tableView = QtWidgets.QTableView(self.dockWidgetContents)
        self.tableView.setEditTriggers(QtWidgets.QAbstractItemView.DoubleClicked|QtWidgets.QAbstractItemView.EditKeyPressed)
        self.tableView.setAlternatingRowColors(True)
        self.tableView.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.tableView.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tableView.setSortingEnabled(True)
        self.tableView.setObjectName("tableView")
        self.tableView.verticalHeader().setCascadingSectionResizes(True)
        self.verticalLayout.addWidget(self.tableView)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.button_new = QtWidgets.QPushButton(self.dockWidgetContents)
        self.button_new.setObjectName("button_new")
        self.horizontalLayout.addWidget(self.button_new)
        self.button_edit = QtWidgets.QPushButton(self.dockWidgetContents)
        self.button_edit.setObjectName("button_edit")
        self.horizontalLayout.addWidget(self.button_edit)
        self.button_copy = QtWidgets.QPushButton(self.dockWidgetContents)
        self.button_copy.setObjectName("button_copy")
        self.horizontalLayout.addWidget(self.button_copy)
        self.button_delete = QtWidgets.QPushButton(self.dockWidgetContents)
        self.button_delete.setObjectName("button_delete")
        self.horizontalLayout.addWidget(self.button_delete)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        DockWidgetTransfers.setWidget(self.dockWidgetContents)

        self.retranslateUi(DockWidgetTransfers)
        QtCore.QMetaObject.connectSlotsByName(DockWidgetTransfers)

    def retranslateUi(self, DockWidgetTransfers):
        _translate = QtCore.QCoreApplication.translate
        DockWidgetTransfers.setWindowTitle(_translate("DockWidgetTransfers", "&Transfers"))
        self.label.setText(_translate("DockWidgetTransfers", "Filter by:"))
        self.button_new.setText(_translate("DockWidgetTransfers", "New"))
        self.button_edit.setText(_translate("DockWidgetTransfers", "Edit"))
        self.button_copy.setText(_translate("DockWidgetTransfers", "Copy"))
        self.button_delete.setText(_translate("DockWidgetTransfers", "Delete"))

