# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'units.ui'
#
# Created by: PyQt5 UI code generator 5.4.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DockWidgetUnits(object):
    def setupUi(self, DockWidgetUnits):
        DockWidgetUnits.setObjectName("DockWidgetUnits")
        DockWidgetUnits.resize(400, 300)
        self.dockWidgetContents = QtWidgets.QWidget()
        self.dockWidgetContents.setObjectName("dockWidgetContents")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.dockWidgetContents)
        self.verticalLayout.setObjectName("verticalLayout")
        self.tableWidget = QtWidgets.QTableWidget(self.dockWidgetContents)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(1)
        self.tableWidget.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.verticalLayout.addWidget(self.tableWidget)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.button_new = QtWidgets.QPushButton(self.dockWidgetContents)
        self.button_new.setObjectName("button_new")
        self.horizontalLayout.addWidget(self.button_new)
        self.button_edit = QtWidgets.QPushButton(self.dockWidgetContents)
        self.button_edit.setObjectName("button_edit")
        self.horizontalLayout.addWidget(self.button_edit)
        self.button_delete = QtWidgets.QPushButton(self.dockWidgetContents)
        self.button_delete.setObjectName("button_delete")
        self.horizontalLayout.addWidget(self.button_delete)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        DockWidgetUnits.setWidget(self.dockWidgetContents)

        self.retranslateUi(DockWidgetUnits)
        QtCore.QMetaObject.connectSlotsByName(DockWidgetUnits)

    def retranslateUi(self, DockWidgetUnits):
        _translate = QtCore.QCoreApplication.translate
        DockWidgetUnits.setWindowTitle(_translate("DockWidgetUnits", "Uni&ts"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("DockWidgetUnits", "Name"))
        self.button_new.setText(_translate("DockWidgetUnits", "New"))
        self.button_edit.setText(_translate("DockWidgetUnits", "Edit"))
        self.button_delete.setText(_translate("DockWidgetUnits", "Delete"))

