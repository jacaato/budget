# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'budget.ui'
#
# Created by: PyQt5 UI code generator 5.4.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(665, 453)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 665, 29))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuView = QtWidgets.QMenu(self.menubar)
        self.menuView.setObjectName("menuView")
        MainWindow.setMenuBar(self.menubar)
        self.actionExit = QtWidgets.QAction(MainWindow)
        self.actionExit.setObjectName("actionExit")
        self.actionNewTransaction = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon.fromTheme("edit-table-insert-row-below")
        self.actionNewTransaction.setIcon(icon)
        self.actionNewTransaction.setObjectName("actionNewTransaction")
        self.actionEditTransaction = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon.fromTheme("edit-rename")
        self.actionEditTransaction.setIcon(icon)
        self.actionEditTransaction.setObjectName("actionEditTransaction")
        self.actionDeleteTransaction = QtWidgets.QAction(MainWindow)
        icon = QtGui.QIcon.fromTheme("edit-table-delete-row")
        self.actionDeleteTransaction.setIcon(icon)
        self.actionDeleteTransaction.setObjectName("actionDeleteTransaction")
        self.actionCategories = QtWidgets.QAction(MainWindow)
        self.actionCategories.setCheckable(True)
        self.actionCategories.setObjectName("actionCategories")
        self.actionUnits = QtWidgets.QAction(MainWindow)
        self.actionUnits.setCheckable(True)
        self.actionUnits.setObjectName("actionUnits")
        self.actionTransactions = QtWidgets.QAction(MainWindow)
        self.actionTransactions.setCheckable(True)
        self.actionTransactions.setObjectName("actionTransactions")
        self.actionStatistics = QtWidgets.QAction(MainWindow)
        self.actionStatistics.setCheckable(True)
        self.actionStatistics.setObjectName("actionStatistics")
        self.menuFile.addAction(self.actionExit)
        self.menuView.addAction(self.actionTransactions)
        self.menuView.addAction(self.actionStatistics)
        self.menuView.addAction(self.actionCategories)
        self.menuView.addAction(self.actionUnits)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuView.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Budget"))
        self.menuFile.setTitle(_translate("MainWindow", "&File"))
        self.menuView.setTitle(_translate("MainWindow", "&Window"))
        self.actionExit.setText(_translate("MainWindow", "&Exit"))
        self.actionNewTransaction.setText(_translate("MainWindow", "&New"))
        self.actionNewTransaction.setShortcut(_translate("MainWindow", "Ctrl+N"))
        self.actionEditTransaction.setText(_translate("MainWindow", "&Edit"))
        self.actionEditTransaction.setShortcut(_translate("MainWindow", "Ctrl+E"))
        self.actionDeleteTransaction.setText(_translate("MainWindow", "&Delete"))
        self.actionDeleteTransaction.setToolTip(_translate("MainWindow", "Delete"))
        self.actionDeleteTransaction.setShortcut(_translate("MainWindow", "Ctrl+R"))
        self.actionCategories.setText(_translate("MainWindow", "&Categories"))
        self.actionUnits.setText(_translate("MainWindow", "&Units"))
        self.actionTransactions.setText(_translate("MainWindow", "&Transactions"))
        self.actionStatistics.setText(_translate("MainWindow", "&Statistics"))

