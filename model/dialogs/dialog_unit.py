from PyQt5.QtWidgets import *

from view import ui_dialog_unit


class UnitDialog(QDialog):
    def __init__(self, parent=None, name=None):
        super(UnitDialog, self).__init__(parent)
        self.ui = ui_dialog_unit.Ui_DialogUnit()
        self.ui.setupUi(self)
        if name is not None:
            self.ui.edit_name.setText(name)

    def accept(self):
        name = self.ui.edit_name.text()
        if len(name) == 0:
            QMessageBox.warning(self, "Empty name", "Name can not be empty!")
            self.ui.edit_name.setFocus()
            return
        QDialog.accept(self)

    def get_name(self):
        return self.ui.edit_name.text()
