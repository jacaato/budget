import datetime

from PyQt5.QtCore import QDate, Qt
from PyQt5.QtWidgets import *
from database import db_orm
from view import ui_dialog_transaction


class TransactionDialog(QDialog):
    def __init__(self, parent=None, name=None, amount=None, value=None, date=None, unit=None, category=None, model=None):
        super(TransactionDialog, self).__init__(parent)
        self.ui = ui_dialog_transaction.Ui_DialogTransaction()
        self.ui.setupUi(self)

        self.ui.pushButton_today.clicked.connect(self.set_today)

        self.ui.combo_category.addItem("No category", None)
        for c in db_orm.session.query(db_orm.Category).all():
            self.ui.combo_category.addItem(c.name, c)

        self.ui.combo_unit.addItem("No unit", None)
        for u in db_orm.session.query(db_orm.Unit).all():
            self.ui.combo_unit.addItem(u.name, u)

        self.ui.edit_date.setDate(datetime.date.today())

        if name is not None:
            self.ui.edit_name.setText(name)
        if amount is not None:
            self.ui.spin_box_amount.setValue(amount)
        if value is not None:
            self.ui.spin_box_value.setValue(value)
        if date is not None:
            self.ui.edit_date.setDate(date)
        if unit is not None:
            self.ui.combo_unit.setCurrentText(unit.name)
        if category is not None:
            self.ui.combo_category.setCurrentText(category.name)

        self.model = model
        if self.model:
            self._set_completer()

    def _set_completer(self):
        completer = QCompleter(self.model, self)

        completer.setCompletionColumn(0)
        completer.setCompletionRole(Qt.DisplayRole)
        completer.setCaseSensitivity(Qt.CaseInsensitive)
        completer.setFilterMode(Qt.MatchContains)
        completer.setWrapAround(False)
        # TODO remove duplicates from completer
        self.ui.edit_name.setCompleter(completer)

    def accept(self):
        name = self.ui.edit_name.text()
        if len(name) == 0:
            QMessageBox.warning(self, "Empty name", "Name can not be empty!")
            self.ui.edit_name.setFocus()
            return
        QDialog.accept(self)

    def get_name(self):
        return self.ui.edit_name.text()

    def get_amount(self):
        return self.ui.spin_box_amount.value()

    def get_value(self):
        return self.ui.spin_box_value.value()

    def get_date(self):
        return self.ui.edit_date.date().toPyDate()

    def get_unit(self):
        return self.ui.combo_unit.itemData(self.ui.combo_unit.currentIndex())

    def get_category(self):
        return self.ui.combo_category.itemData(self.ui.combo_category.currentIndex())

    def set_today(self):
        self.ui.edit_date.setDate(QDate.currentDate())
