from PyQt5.QtWidgets import *

from database import db_orm
from view import ui_dialog_category


class CategoryDialog(QDialog):
    def __init__(self, parent=None, name=None, parent_category=None):
        super(CategoryDialog, self).__init__(parent)
        self.ui = ui_dialog_category.Ui_DialogCategory()
        self.ui.setupUi(self)
        if name is not None:
            self.ui.edit_name.setText(name)

        self.ui.combo_parent.addItem("None", None)
        for category in db_orm.session.query(db_orm.Category).all():
            if category.name != name:
                self.ui.combo_parent.addItem(category.name, category)

        if parent_category is not None:
            self.ui.combo_parent.setCurrentText(parent_category.name)

    def accept(self):
        if len(self.ui.edit_name.text()) == 0:
            QMessageBox.warning(self, "Empty name", "Name can not be empty!")
            self.ui.edit_name.setFocus()
            return
        QDialog.accept(self)

    def get_name(self):
        return self.ui.edit_name.text()

    def get_parent_category(self):
        return self.ui.combo_parent.itemData(self.ui.combo_parent.currentIndex())
