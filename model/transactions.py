from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDockWidget, QDialog, QMessageBox

from database import db_orm
from model.dialogs import dialog_transaction
from model.item_delegates import transaction_date_delegate, transaction_unit_delegate, transaction_category_delegate, \
    transaction_value_delegate, transaction_name_delegate
from model.models.transaction_model import TransactionModel
from model.models.transaction_sort_filter_proxy_model import TransactionSortProxyModel
from view import ui_transactions


class TransactionsWidget(QDockWidget):
    def __init__(self, parent=None):
        super(TransactionsWidget, self).__init__(parent)
        self.ui = ui_transactions.Ui_DockWidgetTransfers()
        self.ui.setupUi(self)

        self.ui.button_new.clicked.connect(self.click_new)
        self.ui.button_edit.clicked.connect(self.click_edit)
        self.ui.button_delete.clicked.connect(self.click_delete)
        self.ui.button_copy.clicked.connect(self.click_copy)

        self.model = TransactionSortProxyModel(self)
        self.model.setSourceModel(TransactionModel())
        self.ui.tableView.setModel(self.model)

        self.ui.tableView.setItemDelegateForColumn(0, transaction_name_delegate.TransactionNameDelegate(self))
        self.ui.tableView.setItemDelegateForColumn(1, transaction_value_delegate.TransactionValueDelegate(self))
        self.ui.tableView.setItemDelegateForColumn(2, transaction_value_delegate.TransactionValueDelegate(self))
        self.ui.tableView.setItemDelegateForColumn(3, transaction_unit_delegate.TransactionUnitDelegate(self))
        self.ui.tableView.setItemDelegateForColumn(4, transaction_category_delegate.TransactionCategoryDelegate(self))
        self.ui.tableView.setItemDelegateForColumn(5, transaction_date_delegate.TransactionDateDelegate(self))

        self.ui.tableView.sortByColumn(5, Qt.AscendingOrder)
        self.ui.tableView.resizeColumnToContents(0)
        self.ui.tableView.resizeColumnToContents(1)
        self.ui.tableView.resizeColumnToContents(2)
        self.ui.tableView.resizeColumnToContents(3)
        self.ui.tableView.resizeColumnToContents(4)
        self.ui.tableView.resizeColumnToContents(5)

        # init filter drop-down
        for i in range(self.ui.tableView.model().columnCount()):
            name = self.ui.tableView.model().headerData(i, Qt.Horizontal)
            self.ui.comboBox_column_filter.addItem(name)

        self.ui.comboBox_column_filter.currentTextChanged.connect(self.filter_data)
        self.ui.lineEdit_filter_by.textChanged.connect(self.filter_data)

    def closeEvent(self, QCloseEvent):
        for action in self.actions():
            if action.objectName() == 'actionTransactions':
                action.setChecked(False)

    def showEvent(self, QShowEvent):
        for action in self.actions():
            if action.objectName() == 'actionTransactions':
                action.setChecked(True)

    def filter_data(self):
        type_ = self.ui.comboBox_column_filter.currentText()
        text = self.ui.lineEdit_filter_by.text()
        self.ui.tableView.model().set_filter(type_, text)

    def cell_changed(self, row):
        item = self.ui.tableView.item(row, 0).data(Qt.UserRole)
        item.name = self.ui.tableView.item(row, 0).text()
        item.amount = self.ui.tableView.item(row, 1).text()
        item.value = self.ui.tableView.item(row, 2).text()
        item.unit = self.ui.tableView.item(row, 3).data(Qt.UserRole)
        item.category = self.ui.tableView.item(row, 4).data(Qt.UserRole)
        item.date = self.ui.tableView.item(row, 5).data(Qt.UserRole)
        item.unit_id = item.unit.id if item.unit is not None else None
        item.category_id = item.category.id if item.category is not None else None
        self.ui.tableView.resizeColumnsToContents()
        db_orm.session.add(item)
        db_orm.session.commit()

    def click_new(self):
        dialog = dialog_transaction.TransactionDialog(self, model=self.model)
        if dialog.exec_() == QDialog.Accepted:
            name = dialog.get_name()
            amount = dialog.get_amount()
            value = dialog.get_value()
            date = dialog.get_date()
            unit = dialog.get_unit()
            category = dialog.get_category()
            unit_id = unit.id if unit is not None else None
            category_id = category.id if category is not None else None

            new_item = db_orm.Transaction(name, value, date, amount, unit_id, unit, category_id, category)

            self.ui.tableView.model().add_data(0, new_item, self.ui.tableView.model().index(0, 0))

    def click_delete(self):
        row = self.ui.tableView.currentIndex().row()
        item = self.ui.tableView.model().data(self.ui.tableView.currentIndex(), Qt.UserRole)
        if not item:
            return
        if QMessageBox.question(self, 'Delete',
                                'Do you really want to delete transaction '
                                '{}, value={}, date={} ?'.format(item.name, item.value, item.date)) == QMessageBox.Yes:
            self.ui.tableView.model().remove_data(row, self.ui.tableView.model().index(row, 0))

    def click_edit(self):
        item = self.ui.tableView.model().data(self.ui.tableView.currentIndex(), Qt.UserRole)
        if not item:
            return

        dialog = dialog_transaction.TransactionDialog(self, item.name, float(item.amount), item.value, item.date,
                                                      item.unit,
                                                      item.category, model=self.model)
        if dialog.exec_() == QDialog.Accepted:
            item.name = dialog.get_name()
            item.amount = dialog.get_amount()
            item.value = dialog.get_value()
            item.date = dialog.get_date()
            item.unit = dialog.get_unit()
            item.category = dialog.get_category()
            item.unit_id = item.unit.id if item.unit is not None else None
            item.category_id = item.category.id if item.category is not None else None

            self.ui.tableView.model().setData(self.ui.tableView.currentIndex(), item)

    def click_copy(self):
        item = self.ui.tableView.model().data(self.ui.tableView.currentIndex(), Qt.UserRole)
        if not item:
            return

        dialog = dialog_transaction.TransactionDialog(self, item.name, float(item.amount), item.value, item.date,
                                                      item.unit,
                                                      item.category, model=self.model)
        if dialog.exec_() == QDialog.Accepted:
            name = dialog.get_name()
            amount = dialog.get_amount()
            value = dialog.get_value()
            date = dialog.get_date()
            unit = dialog.get_unit()
            category = dialog.get_category()
            unit_id = unit.id if unit is not None else None
            category_id = category.id if category is not None else None

            new_item = db_orm.Transaction(name, value, date, amount, unit_id, unit, category_id, category)

            self.ui.tableView.model().add_data(0, new_item, self.ui.tableView.model().index(0, 0))
