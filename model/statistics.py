import os
import abc
import time

from PyQt5.QtCore import Qt, QDate
from PyQt5.QtWidgets import QDockWidget, QTreeWidgetItem
from matplotlib import pyplot

from database import db_orm
from view import ui_statistics


def linear_regression(x, y, zero_start=False):
    """
    :param x: array of dates
    :param y: array of values
    :param zero_start: indicates if we want y=ax instead of y=ax+b
    :return: (a,b) where y=ax+b
    """

    sum_x = 0
    sum_y = y[0]
    sum_xy = 0
    sum_sqr_x = 0

    for i in range(1, len(x)):
        day_diff = x[i] - x[0]  # we need current x to be difference in days
        sum_x += day_diff.days
        sum_y += y[i]
        sum_xy += day_diff.days * y[i]
        sum_sqr_x += day_diff.days ** 2

    a = float(len(x) * sum_xy - sum_x * sum_y) / float(len(x) * sum_sqr_x - sum_x ** 2)
    b = 0  # if we start from 0
    if not zero_start:
        b = 1.0 / len(x) * (sum_y - a * sum_x)
    return a, b


class BaseChart(object):
    image_name = 'chart.png'

    def __init__(self, ui):
        self.ui = ui

    @abc.abstractclassmethod
    def refresh(self, selected_categories, start_date, stop_date):
        pass

    def __del__(self):
        os.remove(self.image_name)


class PieBaseChart(BaseChart):
    def refresh(self, selected_categories, start_date, stop_date):
        def make_autopct(values):
            def my_autopct(pct):
                total = sum(values)
                val = int((pct * total / 100.0) + 0.5)
                return '{p:.2f}%  ({v:d} zł)'.format(p=pct, v=val)

            return my_autopct

        labels = []
        explode = []
        sizes = []
        for item_category in map(lambda x: x.data(0, Qt.UserRole), selected_categories):
            labels.append(item_category.name)
            value = 0
            for transaction in db_orm.session.query(db_orm.Transaction).filter(
                            db_orm.Transaction.date >= start_date).filter(db_orm.Transaction.date <= stop_date):
                category = transaction.category
                while category is not None:
                    if category == item_category:
                        value += float(transaction.value) * float(transaction.amount)
                    category = category.parent_category

            sizes.append(value)
            explode.append(0.1)

        pyplot.pie(sizes, explode=explode, labels=labels, autopct=make_autopct(sizes), shadow=True, startangle=90,
                   textprops={'backgroundcolor': 'white'})
        pyplot.axis('equal')

        pyplot.savefig(self.image_name, transparent=True)
        pyplot.clf()


class LineBaseChart(BaseChart):
    def refresh(self, selected_categories, start_date, stop_date):
        fig, ax = pyplot.subplots(1)
        fig.autofmt_xdate()
        values = dict()
        for item_category in map(lambda x: x.data(0, Qt.UserRole), selected_categories):
            label = item_category.name
            sum = 0
            for transaction in db_orm.session.query(db_orm.Transaction).filter(
                            db_orm.Transaction.date >= start_date).filter(
                        db_orm.Transaction.date <= stop_date).order_by(db_orm.Transaction.date):
                category = transaction.category
                while category is not None:
                    if category == item_category:
                        if values.get(transaction.date, None) is None:
                            if self.ui.checkBox_cumulative.isChecked():
                                values[transaction.date] = sum
                            else:
                                values[transaction.date] = 0
                        values[transaction.date] += float(transaction.value) * float(transaction.amount)
                        sum = values[transaction.date]
                    category = category.parent_category
            sorted_dates = sorted(values)
            sorted_values = [values[item] for item in sorted_dates]
            ax.plot(sorted_dates, sorted_values, label=label)
            if self.ui.checkBox_linreg.isChecked():
                a, b = linear_regression(sorted_dates, sorted_values, True)
                ax.plot([sorted_dates[0], sorted_dates[-1]], [b, (sorted_dates[-1] - sorted_dates[0]).days * a + b],
                        label=label + " lin reg")
            values.clear()

        ax.legend(loc='upper left')
        pyplot.savefig(self.image_name, transparent=True)
        pyplot.clf()


class BarBaseChart(BaseChart):
    def refresh(self, selected_categories, start_date, stop_date):
        fig, ax = pyplot.subplots(1)
        fig.autofmt_xdate()
        values = dict()
        for item_category in map(lambda x: x.data(0, Qt.UserRole), selected_categories):
            label = item_category.name
            for transaction in db_orm.session.query(db_orm.Transaction).order_by(db_orm.Transaction.date).all():
                category = transaction.category
                while category is not None:
                    if category == item_category:
                        if values.get(transaction.date, None) is None:
                            values[transaction.date] = float(transaction.value) * float(transaction.amount)
                        else:
                            values[transaction.date] += float(transaction.value) * float(transaction.amount)
                    category = category.parent_category
            sorted_dates = sorted(values)
            ax.bar(sorted_dates, [values[item] for item in sorted_dates], label=label)
            values.clear()

        ax.legend(loc='upper left')
        pyplot.savefig(self.image_name, transparent=True)
        pyplot.clf()


class StatisticsWidget(QDockWidget):
    charts_types = {
        'Pie': PieBaseChart,
        'Line': LineBaseChart,
        'Bar': BarBaseChart
    }

    def __init__(self, parent=None):
        super(StatisticsWidget, self).__init__(parent)
        self.ui = ui_statistics.Ui_DockWidgetStatistics()
        self.ui.setupUi(self)

        self.ui.chart_frame.set_image_name(BaseChart.image_name)
        self.chart = None
        self.change_chart_type(self.ui.comboBox_type.currentText())

        date_now = QDate.currentDate()
        date_month = QDate(date_now.year(), date_now.month(), 1)
        self.ui.dateEdit_from.setDate(date_month)
        self.ui.dateEdit_to.setDate(date_now)

        self.update_tree()
        self.refresh_image()

        self.ui.pushButton_refresh.clicked.connect(self.refresh)
        self.ui.treeWidget.itemSelectionChanged.connect(self.refresh_image)
        self.ui.treeWidget.itemSelectionChanged.connect(self.ui.chart_frame.repaint)
        self.ui.comboBox_type.currentTextChanged.connect(self.change_chart_type)
        self.ui.checkBox_cumulative.clicked.connect(self.refresh_image)
        self.ui.checkBox_linreg.clicked.connect(self.refresh_image)
        self.ui.dateEdit_from.dateChanged.connect(self.refresh_image)
        self.ui.dateEdit_to.dateChanged.connect(self.refresh_image)

    def change_chart_type(self, chart_type):
        self.chart = self.charts_types[chart_type](self.ui)
        self.chart.refresh(self.ui.treeWidget.selectedItems(), self.ui.dateEdit_from.date().toString(Qt.ISODate),
                           self.ui.dateEdit_to.date().toString(Qt.ISODate))
        self.ui.chart_frame.repaint()

    def update_tree(self):
        self.ui.treeWidget.clear()
        tree_items = []
        for category in db_orm.session.query(db_orm.Category).all():
            tree_item = QTreeWidgetItem()
            tree_item.setText(0, category.name)
            tree_item.setData(0, Qt.UserRole, category)
            tree_items.append(tree_item)
        for item in tree_items:
            category = item.data(0, Qt.UserRole)
            if category.parent_category_id is not None:
                parent = next(x for x in tree_items if x.data(0, Qt.UserRole).id == category.parent_category_id)
                parent.addChild(item)
            else:
                self.ui.treeWidget.insertTopLevelItem(0, item)

    def refresh(self):
        self.update_tree()
        self.chart.refresh(self.ui.treeWidget.selectedItems(), self.ui.dateEdit_from.date().toString(Qt.ISODate),
                           self.ui.dateEdit_to.date().toString(Qt.ISODate))
        self.ui.chart_frame.repaint()

    def refresh_image(self):
        self.chart.refresh(self.ui.treeWidget.selectedItems(), self.ui.dateEdit_from.date().toString(Qt.ISODate),
                           self.ui.dateEdit_to.date().toString(Qt.ISODate))
        self.ui.chart_frame.repaint()

    def closeEvent(self, QCloseEvent):
        for action in self.actions():
            if action.objectName() == 'actionStatistics':
                action.setChecked(False)

    def showEvent(self, QShowEvent):
        for action in self.actions():
            if action.objectName() == 'actionStatistics':
                action.setChecked(True)
