from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDockWidget, QTableWidgetItem, QDialog, QMessageBox

from database import db_orm
from model.dialogs import dialog_unit
from view import ui_units


class UnitsWidget(QDockWidget):
    def __init__(self, parent=None):
        super(UnitsWidget, self).__init__(parent)
        self.ui = ui_units.Ui_DockWidgetUnits()
        self.ui.setupUi(self)

        self.ui.button_new.clicked.connect(self.click_new)
        self.ui.button_edit.clicked.connect(self.click_edit)
        self.ui.button_delete.clicked.connect(self.click_delete)

        self.ui.tableWidget.cellChanged.connect(self.cell_changed)

        self.update_table()

    def update_table(self):
        self.ui.tableWidget.cellChanged.disconnect(self.cell_changed)
        self.ui.tableWidget.clearContents()
        self.ui.tableWidget.setRowCount(db_orm.session.query(db_orm.Unit.id).count())
        for index, i in enumerate(db_orm.session.query(db_orm.Unit).all()):
            item = QTableWidgetItem(i.name)
            item.setData(Qt.UserRole, i)
            self.ui.tableWidget.setItem(index, 0, item)
        self.ui.tableWidget.resizeColumnsToContents()
        self.ui.tableWidget.cellChanged.connect(self.cell_changed)

    def closeEvent(self, QCloseEvent):
        for action in self.actions():
            if action.objectName() == 'actionUnits':
                action.setChecked(False)

    def showEvent(self, QShowEvent):
        for action in self.actions():
            if action.objectName() == 'actionUnits':
                action.setChecked(True)
        # check dirty mainwindow and update if needed
        self.update_table()

    def cell_changed(self, row):
        item = self.ui.tableWidget.item(row, 0).data(Qt.UserRole)
        item.name = self.ui.tableWidget.item(row, 0).text()
        db_orm.session.add(item)
        db_orm.session.commit()

    def click_new(self):
        dialog = dialog_unit.UnitDialog(self)
        if dialog.exec_() == QDialog.Accepted:
            name = dialog.get_name()
            new_item = db_orm.Unit(name)
            db_orm.session.add(new_item)
            db_orm.session.commit()
            self.update_table()

    def click_delete(self):
        row = self.ui.tableWidget.currentRow()
        if row == -1:
            return
        item = self.ui.tableWidget.item(row, 0).data(Qt.UserRole)
        if QMessageBox.question(self, 'Delete',
                                'Do you really want to delete unit {} ?'.format(item.name)) == QMessageBox.Yes:
            db_orm.session.delete(item)
            db_orm.session.commit()
            self.update_table()

    def click_edit(self):
        row = self.ui.tableWidget.currentRow()
        if row == -1:
            return
        item = self.ui.tableWidget.item(row, 0).data(Qt.UserRole)
        name = self.ui.tableWidget.item(row, 0).text()
        dialog = dialog_unit.UnitDialog(self, name)
        if dialog.exec_() == QDialog.Accepted:
            item.name = dialog.get_name()
            db_orm.session.add(item)
            db_orm.session.commit()
            self.update_table()
