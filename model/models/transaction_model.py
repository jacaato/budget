from PyQt5.QtCore import QAbstractTableModel, Qt, QModelIndex

from database import db_orm
from database.db_orm import Transaction


class TransactionModel(QAbstractTableModel):
    def __init__(self, parent=None):
        super(TransactionModel, self).__init__(parent)
        self._data = db_orm.session.query(db_orm.Transaction).all()

    def add_data(self, position, data, index=QModelIndex()):
        self.beginInsertRows(QModelIndex(), position, position)
        self._data.insert(position, None)
        self._data[position] = data
        db_orm.session.add(data)
        db_orm.session.commit()
        self.endInsertRows()

    def remove_data(self, position, index=QModelIndex()):
        self.beginRemoveRows(QModelIndex(), position, position)
        item = self._data[position]
        db_orm.session.delete(item)
        db_orm.session.commit()
        self._data.remove(item)
        self.endRemoveRows()

    # QT Methods

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self._data)

    def columnCount(self, parent=None, *args, **kwargs):
        return 6

    def flags(self, index):
        return Qt.ItemIsEditable | Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return

        transaction = self._data[index.row()]
        if transaction is None:
            return
        if role == Qt.DisplayRole:
            column = index.column()
            if column == 0:
                return transaction.name
            elif column == 1:
                return float(transaction.amount)
            elif column == 2:
                return float(transaction.value)
            elif column == 3:
                return transaction.unit.name if transaction.unit is not None else "No unit"
            elif column == 4:
                return transaction.category.name if transaction.category is not None else "No category"
            elif column == 5:
                return str(transaction.date)
        elif role == Qt.UserRole:
            return transaction

    def setData(self, index, data, role=None):
        if not index.isValid():
            return False
        if role == Qt.EditRole and not isinstance(data, Transaction):  # delegates
            column = index.column()
            row = index.row()
            transaction = self._data[row]
            if column == 0:
                transaction.name = data
            elif column == 1:
                transaction.amount = data
            elif column == 2:
                transaction.value = data
            elif column == 3:
                transaction.unit = data
            elif column == 4:
                transaction.category = data
            elif column == 5:
                transaction.date = data
            db_orm.session.add(transaction)
        elif role == Qt.DisplayRole or isinstance(data, Transaction):
            self._data[index.row()] = data
            db_orm.session.add(data)

        db_orm.session.commit()
        return True

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                if section == 0:
                    return "Name"
                elif section == 1:
                    return "Amount"
                elif section == 2:
                    return "Value"
                elif section == 3:
                    return "Unit"
                elif section == 4:
                    return "Category"
                elif section == 5:
                    return "Date"
            elif orientation == Qt.Vertical:
                return section + 1
