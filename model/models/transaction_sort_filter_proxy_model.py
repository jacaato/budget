from PyQt5.QtCore import QSortFilterProxyModel, Qt, QModelIndex


class TransactionSortProxyModel(QSortFilterProxyModel):
    def __init__(self, parent=None):
        super(TransactionSortProxyModel, self).__init__(parent)
        self.setSortLocaleAware(True)
        self.setFilterCaseSensitivity(Qt.CaseInsensitive)

    def set_filter(self, type_, value):
        self.setFilterRegExp(value)
        if type_ == 'Name':
            self.setFilterKeyColumn(0)
        elif type_ == 'Amount':
            self.setFilterKeyColumn(1)
        elif type_ == 'Value':
            self.setFilterKeyColumn(2)
        elif type_ == 'Unit':
            self.setFilterKeyColumn(3)
        elif type_ == 'Category':
            self.setFilterKeyColumn(4)
        elif type_ == 'Date':
            self.setFilterKeyColumn(5)

    def add_data(self, position, data, index=QModelIndex()):
        self.sourceModel().add_data(position, data, self.mapToSource(index))

    def remove_data(self, position, index=QModelIndex()):
        self.sourceModel().remove_data(self.mapToSource(self.index(position, 0)).row(), self.mapToSource(index))

    # QT Methods

    def filterAcceptsRow(self, source_row, index=QModelIndex()):
        if self.filterRegExp().pattern() == '':
            return True

        source_index = self.sourceModel().index(source_row, self.filterKeyColumn(), index)

        if not source_index.isValid():
            return False

        row_data = str(source_index.data())

        if self.filterRegExp().indexIn(row_data) != -1:
            return True

        return False

    def lessThan(self, index_left, index_right):
        if index_left.data() is None:
            return False
        elif index_right.data() is None:
            return True
        return index_left.data() > index_right.data()

    # reimplement for not sorting header
    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if orientation != Qt.Vertical or role != Qt.DisplayRole:
            return self.sourceModel().headerData(section, orientation, role)
        return section + 1
