from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDockWidget, QTableWidgetItem, QDialog, QMessageBox

from database import db_orm
from model.item_delegates import category_delegate
from view import ui_categories
from model.dialogs import dialog_category


class CategoriesWidget(QDockWidget):
    def __init__(self, parent=None):
        super(CategoriesWidget, self).__init__(parent, Qt.SubWindow)
        self.ui = ui_categories.Ui_DockWidgetCategoried()
        self.ui.setupUi(self)

        self.ui.button_new.clicked.connect(self.click_new)
        self.ui.button_edit.clicked.connect(self.click_edit)
        self.ui.button_delete.clicked.connect(self.click_delete)

        self.ui.tableWidget_categories.setItemDelegateForColumn(1, category_delegate.CategoryDelegate(1, self))

        self.ui.tableWidget_categories.cellChanged.connect(self.cell_changed)

        self.update_table()

    def update_table(self):
        self.ui.tableWidget_categories.cellChanged.disconnect(self.cell_changed)
        self.ui.tableWidget_categories.clearContents()
        self.ui.tableWidget_categories.setRowCount(db_orm.session.query(db_orm.Category.id).count())
        for index, i in enumerate(db_orm.session.query(db_orm.Category).all()):
            item = QTableWidgetItem(i.name)
            item.setData(Qt.UserRole, i)
            self.ui.tableWidget_categories.setItem(index, 0, item)
            item = QTableWidgetItem("None" if i.parent_category is None else i.parent_category.name)
            item.setData(Qt.UserRole, None if i.parent_category is None else i.parent_category)
            self.ui.tableWidget_categories.setItem(index, 1, item)
        self.ui.tableWidget_categories.resizeColumnsToContents()
        self.ui.tableWidget_categories.cellChanged.connect(self.cell_changed)

    def closeEvent(self, QCloseEvent):
        for action in self.actions():
            if action.objectName() == 'actionCategories':
                action.setChecked(False)

    def showEvent(self, QShowEvent):
        for action in self.actions():
            if action.objectName() == 'actionCategories':
                action.setChecked(True)
        self.update_table()

    def cell_changed(self, row):
        item = self.ui.tableWidget_categories.item(row, 0).data(Qt.UserRole)
        item.name = item.name
        item.parent_category = self.ui.tableWidget_categories.item(row, 1).data(Qt.UserRole)
        item.parent_category_id = item.parent_category.id if item.parent_category is not None else None
        db_orm.session.add(item)
        db_orm.session.commit()

    def click_new(self):
        dialog = dialog_category.CategoryDialog(self)
        if dialog.exec_() == QDialog.Accepted:
            name = dialog.get_name()
            parent = dialog.get_parent_category()
            if parent is not None:
                new_item = db_orm.Category(name, parent.id, parent)
            else:
                new_item = db_orm.Category(name)
            db_orm.session.add(new_item)
            db_orm.session.commit()
            self.update_table()

    def click_delete(self):
        row = self.ui.tableWidget_categories.currentRow()
        if row == -1:
            return
        item = self.ui.tableWidget_categories.item(row, 0).data(Qt.UserRole)
        if QMessageBox.question(self, 'Delete',
                                'Do you really want to delete category {} ?'.format(item.name)) == QMessageBox.Yes:
            db_orm.session.delete(item)
            db_orm.session.commit()
            self.update_table()

    def click_edit(self):
        row = self.ui.tableWidget_categories.currentRow()
        if row == -1:
            return
        item = self.ui.tableWidget_categories.item(row, 0).data(Qt.UserRole)
        name = item.name
        parent = item.parent_category
        dialog = dialog_category.CategoryDialog(self, name, parent)
        if dialog.exec_() == QDialog.Accepted:
            item.name = dialog.get_name()
            if dialog.get_parent_category() is not None:
                item.parent_category_id = dialog.get_parent_category().id
                item.parent_category = dialog.get_parent_category()
            else:
                item.parent_category_id = None
                item.parent_category = None
            db_orm.session.add(item)
            db_orm.session.commit()
            self.update_table()
