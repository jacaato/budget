from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QComboBox, QItemDelegate

from database import db_orm


class CategoryDelegate(QItemDelegate):
    def __init__(self, column, parent=None):
        super(CategoryDelegate, self).__init__(parent)
        self.editor = None
        self.column = column

    def createEditor(self, parent, style, index):
        self.editor = QComboBox(parent)
        self.editor.addItem("None", None)
        for item in db_orm.session.query(db_orm.Category).all():
            if item.name != index.sibling(index.row(), 0).data(Qt.DisplayRole):
                self.editor.addItem(item.name, item)

        return self.editor

    def setModelData(self, editor, item, index):
        current_category = editor.currentData(Qt.UserRole)
        item.setItemData(index, {Qt.UserRole: current_category,
                                 Qt.DisplayRole: current_category.name if current_category is not None else "None"})
