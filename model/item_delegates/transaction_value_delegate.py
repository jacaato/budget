from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QItemDelegate, QDoubleSpinBox


class TransactionValueDelegate(QItemDelegate):
    def __init__(self, parent=None):
        super(TransactionValueDelegate, self).__init__(parent)
        self.editor = None

    def createEditor(self, parent, style, index):
        self.editor = QDoubleSpinBox(parent)
        self.editor.setDecimals(3)
        self.editor.setRange(-1000000.00, 1000000.00)
        return self.editor

    def setEditorData(self, editor, index):
        editor.setValue(float(index.data()))
