from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QComboBox, QItemDelegate

from database import db_orm


class TransactionCategoryDelegate(QItemDelegate):
    def __init__(self, parent=None):
        super(TransactionCategoryDelegate, self).__init__(parent)
        self.editor = None

    def createEditor(self, parent, style, index):
        self.editor = QComboBox(parent)
        self.editor.addItem("None", None)
        for item in db_orm.session.query(db_orm.Category).all():
            self.editor.addItem(item.name, item)
        return self.editor

    def setEditorData(self, editor, index):
        self.editor.setCurrentText(index.data())

    def setModelData(self, editor, item, index):
        current_category = editor.currentData(Qt.UserRole)
        item.setItemData(index, {Qt.EditRole: current_category})
