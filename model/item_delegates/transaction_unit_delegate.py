from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QComboBox, QItemDelegate

from database import db_orm


class TransactionUnitDelegate(QItemDelegate):
    def __init__(self, parent=None):
        super(TransactionUnitDelegate, self).__init__(parent)
        self.editor = None

    def createEditor(self, parent, style, index):
        self.editor = QComboBox(parent)
        self.editor.addItem("No unit", None)
        for item in db_orm.session.query(db_orm.Unit).all():
            self.editor.addItem(item.name, item)
        return self.editor

    def setEditorData(self, editor, index):
        self.editor.setCurrentText(index.data())

    def setModelData(self, editor, item, index):
        current_unit = editor.currentData(Qt.UserRole)
        item.setItemData(index, {Qt.EditRole: current_unit})
