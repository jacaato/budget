from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDateEdit, QItemDelegate


class TransactionDateDelegate(QItemDelegate):
    def __init__(self, parent=None):
        super(TransactionDateDelegate, self).__init__(parent)
        self.editor = None

    def createEditor(self, parent, style, index):
        self.editor = QDateEdit(index.data(Qt.UserRole).date, parent)
        return self.editor

    def setModelData(self, editor, item, index):
        date = editor.date()
        item.setItemData(index, {Qt.EditRole: date.toPyDate()})
