from PyQt5.QtWidgets import QItemDelegate, QLineEdit


class TransactionNameDelegate(QItemDelegate):
    def __init__(self, parent=None):
        super(TransactionNameDelegate, self).__init__(parent)
        self.editor = None

    def createEditor(self, parent, style, index):
        self.editor = QLineEdit(parent)
        return self.editor

    def setEditorData(self, editor, index):
        editor.setText(index.data())
