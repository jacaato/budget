#!/usr/bin/python3

import sys

from PyQt5.QtCore import QSettings
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon

from view import ui_budget
from model import statistics, units, categories, transactions


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        self.ui = ui_budget.Ui_MainWindow()
        self.ui.setupUi(self)
        self.setDockNestingEnabled(True)

        self.transaction_widget = transactions.TransactionsWidget(self)
        self.transaction_widget.addAction(self.ui.actionTransactions)
        self.transaction_widget.hide()

        self.categories_widget = categories.CategoriesWidget(self)
        self.categories_widget.addAction(self.ui.actionCategories)
        self.categories_widget.hide()

        self.units_widget = units.UnitsWidget(self)
        self.units_widget.addAction(self.ui.actionUnits)
        self.units_widget.hide()

        self.statistics_widget = statistics.StatisticsWidget(self)
        self.statistics_widget.addAction(self.ui.actionStatistics)
        self.statistics_widget.hide()

        self.ui.actionCategories.toggled.connect(self.categories_widget.setVisible)
        self.ui.actionUnits.toggled.connect(self.units_widget.setVisible)
        self.ui.actionTransactions.toggled.connect(self.transaction_widget.setVisible)
        self.ui.actionStatistics.toggled.connect(self.statistics_widget.setVisible)

        self.ui.actionExit.triggered.connect(self.file_exit)

        self.settings = QSettings()
        if self.settings.value("WINDOW_STATE") is not None:
            self.restoreGeometry(self.settings.value("GEOMETRY"))
            self.restoreState(self.settings.value("WINDOW_STATE"), 1)

    def file_exit(self):
        self.close()

    def closeEvent(self, QCloseEvent):
        self.settings.setValue("GEOMETRY", self.saveGeometry())
        self.settings.setValue("WINDOW_STATE", self.saveState(1))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon('icon.png'))
    app.setApplicationName("Budget")
    app.setApplicationDisplayName("Budget")
    app.setApplicationVersion("0.10")
    app.setOrganizationName("jacaato")

    form = MainWindow()
    form.show()

    app.exec_()
