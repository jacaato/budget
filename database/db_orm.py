from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date, ForeignKey, create_engine
from sqlalchemy.orm import Session, relationship
import inspect, os

__run_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))

__engine = create_engine("sqlite:///"+__run_path+"/budget.db", echo=False)
#__engine = create_engine("sqlite:///" + __run_path + "/budget_test.db", echo=False)
#__engine = create_engine("sqlite:///" + __run_path + "/budget_test2.db", echo=False)

session = Session(__engine, autoflush=False, autocommit=False)
__Base = declarative_base()

class Unit(__Base):
    __tablename__ = 'units'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __init__(self, name, id=None):
        self.id = id
        self.name = name

    def __repr__(self):
        return "Unit({},{})".format(self.id, self.name)


class Category(__Base):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    parent_category_id = Column(Integer, ForeignKey('categories.id', ondelete='SET NULL'))

    parent_category = relationship("Category", uselist=False, post_update=True, remote_side=[id])

    def __init__(self, name, parent_category_id=None, parent_category=None, id=None):
        self.id = id
        self.name = name
        self.parent_category_id = parent_category_id
        self.parent_category = parent_category

    def __repr__(self):
        return "Category({},{},{},{})".format(self.name, self.parent_category_id, self.parent_category, self.id)


class Transaction(__Base):
    __tablename__ = 'transactions'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    value = Column(String)
    amount = Column(String)
    date = Column(Date)
    unit_id = Column(Integer, ForeignKey('units.id', ondelete='SET DEFAULT'))
    category_id = Column(Integer, ForeignKey('categories.id', ondelete='SET DEFAULT'))

    category = relationship("Category", backref="Transaction")
    unit = relationship("Unit", backref="Transaction")

    def __init__(self, name, value, date, amount, unit_id=None, unit=None, category_id=None, category=None, id=None):
        self.id = id
        self.name = name
        self.value = value
        self.date = date
        self.amount = amount
        self.category_id = category_id
        self.category = category
        self.unit_id = unit_id
        self.unit = unit

    def __repr__(self):
        return "Transaction({},{},{},{},{},{},{},{},{})".format(self.name, self.value, self.date, self.amount,
                                                                self.unit_id, self.unit, self.category_id,
                                                                self.category,
                                                                self.id)

__Base.metadata.create_all(__engine)
